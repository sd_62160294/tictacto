/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.oxprogram;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Tictacto {
    
    static int countRound = 9;
    static char win = '-';
    static boolean isEnd = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWalcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");

    }

    static void input() {
        for (;;) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] != '-') {
                System.out.println("Table at row and col is not empty");
            } else {
                table[row][col] = player;
                countRound--;
                break;
            }
        }
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isEnd = true;
        win = player;
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isEnd = true;
        win = player;
    }

    static void checkX() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != player) {
                return;
            }
        }
        isEnd = true;
        win = player;
    }

    static void checkY() {
        for (int row = 0, count = 2; row < 3; row++, count--) {
            if (table[row][count] != player) {
                return;
            }
        }
        isEnd = true;
        win = player;
    }
        

    static void checkRound() {
        
        if (countRound == 0) {
            isEnd = true;
            win = '-';
        }

    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkY();
        checkRound();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (win == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println("Player " + win + " win...");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");

    }

    public static void main(String[] args) {
        showWalcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isEnd);
        showResult();
        showBye();
    }

}
